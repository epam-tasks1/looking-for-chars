﻿using System;

namespace LookingForChars
{
    public static class CharsCounter
    {
        public static int GetCharsCount(string str, char[] chars)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            int charsCount = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                for (int j = 0; j < str.Length; j++)
                {
                    if (chars[i] == str[j])
                    {
                        charsCount++;
                    }
                }
            }

            return charsCount;
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (startIndex + (endIndex - startIndex) + 1 > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            int currentCharIndex = 0;
            int charsCount = 0;
            while (currentCharIndex < chars.Length)
            {
                int currentStringIndex = startIndex;
                while (currentStringIndex <= endIndex)
                {
                    if (chars[currentCharIndex] == str[currentStringIndex])
                    {
                        charsCount++;
                    }

                    currentStringIndex++;
                }

                currentCharIndex++;
            }

            return charsCount;
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (limit < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex));
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (startIndex + (endIndex - startIndex) + 1 > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            int currentCharIndex = 0;
            int charsCount = 0;
            do
            {
                int currentStringIndex = startIndex;
                do
                {
                    if (chars[currentCharIndex] == str[currentStringIndex])
                    {
                        charsCount++;
                        if (charsCount == limit)
                        {
                            return charsCount;
                        }
                    }

                    currentStringIndex++;
                }
                while (currentStringIndex <= endIndex);
                currentCharIndex++;
            }
            while (currentCharIndex < chars.Length);
            return charsCount;
        }
    }
}
